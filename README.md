# Ayuda Humanitaria a las personas emigrantes

Hablaremos acerca de lo beneficioso y positivo que es la ayuda humanitaria a los emigrantes, pasos a seguir de cómo podemos aportar nuestro granito de arena. Además de explicar cómo es el sufrimiento diario de estas personas y como afectan las mismas